// Module intended to be included by most of the other modules as it provides
// basic, general-purpose functionality lacking in libc. Symbols exported by
// this module thus do not have a "namespace prefix" on purpose.
#ifndef PRELUDE_H
#define PRELUDE_H

#include <sys/types.h> // (ssize_t)


int isempty (char*);


// The outcome of a function call usually used as return value of functions that
// may fail with an error; typedef'd for convenience
enum outcome
  { failure = -1
  , success =  0
  };
typedef enum outcome outcome_t;

// This is guaranteed to not overlap any standard errno value by the C99
// standard. Returned by 'read_chars' when an unexpected EOF is encountered on
// the given descriptor.
#define EUEOF -16

// wrapper for 'strerror' that also supports 'EUEOF'
const char* prelude_strerror (int);

// Read/write the specified amount of characters from/to the given descriptor,
// ensuring no partial transfers by retrying on 'EINTR's. On success, 'success'
// is returned. On errors 'failure' is returned and 'errno' is set
// appropriately. 'write_chars' also ignores 'SIGPIPE' for the duration of the
// call so that 'errno' is set to 'EPIPE' instead of raising a 'SIGPIPE'
// ('sigaction' is used, NOT 'signal').
outcome_t read_chars  (int, ssize_t, char*);
outcome_t write_chars (int, ssize_t, const char*);


#endif // PRELUDE_H
