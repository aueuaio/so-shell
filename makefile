# top level makefile for so-shell

# TODO assert needed make version
# TODO use posix make subst macro expansion instead of cumbersome long lists

CC = gcc -std=c99 -D_POSIX_SOURCE
CFLAGS = -g -Wall -Wextra -pedantic # -O?

all: so-shell-cli so-shell-dem

#TODO include readline from /opt only if on macos; add instructions in README.md
#on where to install readline for macos users

so-shell-cli: cli_main.o prelude.o protocol.o
	$(CC) $(CFLAGS) -o $@ $^ -L/usr/local/opt/readline/lib -lreadline

so-shell-dem: srv_mains.o prelude.o core.o protocol.o
	$(CC) $(CFLAGS) -o $@ $^ -pthread

.c.o:
	$(CC) $(CFLAGS) -I/usr/local/opt/readline/include -I. -o $*.o -c $*.c
	gcc -I. -MM -MT $*.o $*.c > $*.d

deps = srv_mains.d cli_main.d prelude.d core.d protocol.d
objs = srv_mains.o cli_main.o prelude.o core.o protocol.o

# TODO is the '-' a GNU make feature?
-include $(deps)

report.pdf: report.md
	pandoc $^ -o $@

clean:
	! test -f so-shell-dem || rm so-shell-dem
	! test -f so-shell-cli || rm so-shell-cli
	for dep in $(deps); do ! test -f $${dep} || rm $${dep}; done
	for obj in $(objs); do ! test -f $${obj} || rm $${obj}; done
	! test -f report.pdf || rm report.pdf


.PHONY: all clean
